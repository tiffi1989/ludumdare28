package com.me.ludumdare28;

import java.util.Iterator;

import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.tiles.AnimatedTiledMapTile;
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

public class CreateBoxWorld {
	
	private TiledMap map;
	
	public CreateBoxWorld(TiledMap map) {
		this.map = map;
	}

	public World generateWorld(World world) {
		
		Array<StaticTiledMapTile> arrayTiles1 = new Array<StaticTiledMapTile>(2);
		Array<StaticTiledMapTile> arrayTiles2 = new Array<StaticTiledMapTile>(2);
		Array<StaticTiledMapTile> arrayTiles3 = new Array<StaticTiledMapTile>(2);
		Array<StaticTiledMapTile> arrayTiles4 = new Array<StaticTiledMapTile>(2);
		Iterator<TiledMapTile> iterator = map.getTileSets().getTileSet("level2").iterator();
		while(iterator.hasNext()){
			TiledMapTile tile = iterator.next();
			if(tile.getProperties().containsKey("animation1"))
				arrayTiles1.add((StaticTiledMapTile) tile);
			if(tile.getProperties().containsKey("animation2"))
				arrayTiles2.add((StaticTiledMapTile) tile);
			if(tile.getProperties().containsKey("animation3"))
				arrayTiles3.add((StaticTiledMapTile) tile);
			if(tile.getProperties().containsKey("animation4"))
				arrayTiles4.add((StaticTiledMapTile) tile);
			
		}
		
		AnimatedTiledMapTile tileMapTile1 = new AnimatedTiledMapTile(1/3f, arrayTiles1);
		AnimatedTiledMapTile tileMapTile2 = new AnimatedTiledMapTile(1/3f, arrayTiles2);
		AnimatedTiledMapTile tileMapTile3 = new AnimatedTiledMapTile(1/3f, arrayTiles3);
		AnimatedTiledMapTile tileMapTile4 = new AnimatedTiledMapTile(1/3f, arrayTiles4);

		
		
		TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get(1);
		layer.getCell(1, 1);
		for (int x = 0; x < layer.getWidth(); x++) {
			for (int y = 0; y < layer.getHeight(); y++) {
				if (layer.getCell(x, y).getTile().getProperties().containsKey("blocked")) {
					BodyDef rectDef = new BodyDef();
					rectDef.type = BodyType.StaticBody;
					rectDef.position.set(x + 0.5f, y + 0.5f);

					Body rect = world.createBody(rectDef);
					rect.setUserData("wall");

					PolygonShape polyShape = new PolygonShape();
					polyShape.setAsBox(0.5f, 0.5f);

					FixtureDef rectFix = new FixtureDef();
					rectFix.shape = polyShape;

					rect.createFixture(rectFix);
				}
				if(layer.getCell(x, y).getTile().getProperties().containsKey("animation1"))
					layer.getCell(x, y).setTile(tileMapTile1);
				if(layer.getCell(x, y).getTile().getProperties().containsKey("animation2"))
					layer.getCell(x, y).setTile(tileMapTile2);
				if(layer.getCell(x, y).getTile().getProperties().containsKey("animation3"))
					layer.getCell(x, y).setTile(tileMapTile3);
				if(layer.getCell(x, y).getTile().getProperties().containsKey("animation4"))
					layer.getCell(x, y).setTile(tileMapTile4);

			}
		}

		return world;

	}
	
}
