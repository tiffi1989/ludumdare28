package com.me.ludumdare28;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

public class Player {
	int posX, posY;

	int lifes = 100;
	int attack;
	Body body;
	
	public Player(World world, int posX, int posY) {		
		BodyDef playerDef = new BodyDef();
		playerDef.type = BodyType.DynamicBody;
		playerDef.position.set(posX / 18.25f, posY / 18.25f);
		System.out.println(playerDef.position.x + " " + playerDef.position.y);

		body = world.createBody(playerDef);
		body.setUserData("player");

		CircleShape shape = new CircleShape();
		shape.setRadius(0.8f);

		FixtureDef fix = new FixtureDef();
		fix.shape = shape;
		fix.density = 100f;
		fix.friction = 0f;
		fix.restitution = 0f;

		body.createFixture(fix);
	
	}
	
	public float getPosX() {
		return body.getPosition().x;
	}
	
	public float getPosY() {
		return body.getPosition().y;
	}
	
	public int getLifes() {
		return lifes;
	}
	public void setLifes(int lifes) {
		this.lifes = lifes;
	}
	public Body getBody() {
		return body;
	}
	public void setBody(Body body) {
		this.body = body;
	}
	
	public int getRoom() {
		float x = body.getPosition().x;
		float y = body.getPosition().y;

		if (x < 20) {
			if (y > 18)
				return 7;
			else
				return 6;

		}
		if (x > 51)
			return 3;
		if (y > 25 && x >= 20 && x <= 51)
			return 4;
		if (y < 15.5 && x >= 20 && x <= 51)
			return 5;
		if (y >= 15.5 && y <= 25 && x <= 51 && x > 30.5)
			return 1;
		if (y >= 15.5 && y <= 25 && x <= 30.5)
			return 2;

		return attack;

	}
}
