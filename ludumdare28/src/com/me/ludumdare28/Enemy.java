package com.me.ludumdare28;

import box2dLight.PointLight;
import box2dLight.RayHandler;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.World;

public class Enemy {
	int posX, posY;

	int lifes;
	int attack;
	Body body;
	Pathfinding path = new Pathfinding();
	int lastRoom;
	PointLight dieLight;
	FixtureDef fix;

	public Enemy(int lifes, int attack, World world, RayHandler handler, int index) {
		this.lifes = lifes;
		this.attack = attack;

		int room = (int) (Math.random() * 10);

		switch (room) {
		case 0:
			posX = 596;
			posY = Gdx.graphics.getHeight() - 278;
			break;
		case 1:
			if (index != 14) {
				posX = 422;
				posY = Gdx.graphics.getHeight() - 282;
				break;
			}
		case 2:
			posX = 610;
			posY = Gdx.graphics.getHeight() - 126;
			break;
		case 3:
			posX = 577;
			posY = Gdx.graphics.getHeight() - 527;
			break;
		case 4:
			if (index != 10) {
				posX = 300;
				posY = Gdx.graphics.getHeight() - 513;
				break;
			}

		case 5:
			if (index != 17) {
				posX = 300;
				posY = Gdx.graphics.getHeight() - 175;
				break;
			}
		case 6:
			posX = 886;
			posY = Gdx.graphics.getHeight() - 467;
			break;
		case 7:
			if (index != 12) {
				posX = 863;
				posY = Gdx.graphics.getHeight() - 97;
				break;
			}
		case 8:
			posX = 328;
			posY = Gdx.graphics.getHeight() - 99;
			break;
		case 9:
			posX = 394;
			posY = Gdx.graphics.getHeight() - 591;
			break;

		default:
			break;
		}

		BodyDef playerDef = new BodyDef();
		playerDef.type = BodyType.DynamicBody;
		playerDef.position.set(posX / 18.25f, posY / 18.25f);

		body = world.createBody(playerDef);

		CircleShape shape = new CircleShape();
		shape.setRadius(0.75f);

		fix = new FixtureDef();
		fix.shape = shape;
		fix.density = 0.5f;
		fix.friction = 0f;
		fix.restitution = 0f;

		body.createFixture(fix);
		body.setUserData("enemy");

		dieLight = new PointLight(handler, 1000, Color.PINK, 4, 0, 0);
		dieLight.setActive(false);

	}

	public float getPosX() {
		return body.getPosition().x;
	}

	public float getPosY() {
		return body.getPosition().y;
	}

	public int getLifes() {
		return lifes;
	}

	public void setLifes(int lifes) {
		this.lifes = lifes;
	}

	public int getAttack() {
		return attack;
	}

	public void setAttack(int attack) {
		this.attack = attack;
	}

	public void move(Player player) {
		float targetX = 0, targetY = 0;
		if (getRoom() == player.getRoom()) {
			targetX = player.getPosX();
			targetY = player.getPosY();
		} else {
			if (getRoom() == 7) {
				targetX = Pathfinding.door74.x;
				targetY = Pathfinding.door74.y;
			}
			if (getRoom() == 6) {
				targetX = Pathfinding.door65.x;
				targetY = Pathfinding.door65.y;
			}
			if (getRoom() == 3) {
				targetX = Pathfinding.door13.x;
				targetY = Pathfinding.door13.y;
			}
			if (getRoom() == 2) {
				targetX = Pathfinding.door21.x;
				targetY = Pathfinding.door21.y;
			}
			if (getRoom() == 4) {
				if (player.getRoom() == 7) {
					targetX = Pathfinding.door74.x;
					targetY = Pathfinding.door74.y;
				} else {
					targetX = Pathfinding.door41.x;
					targetY = Pathfinding.door41.y;
				}
			}
			if (getRoom() == 5) {
				if (player.getRoom() == 6) {
					targetX = Pathfinding.door65.x;
					targetY = Pathfinding.door65.y;
				} else {
					targetX = Pathfinding.door51.x;
					targetY = Pathfinding.door51.y;
				}
			}
			if (getRoom() == 1) {
				if (player.getRoom() == 3) {
					targetX = Pathfinding.door13.x;
					targetY = Pathfinding.door13.y;
				}
				if (player.getRoom() == 2) {
					targetX = Pathfinding.door21.x;
					targetY = Pathfinding.door21.y;
				}
				if (player.getRoom() == 7 || player.getRoom() == 4) {
					targetX = Pathfinding.door41.x;
					targetY = Pathfinding.door41.y;
				}
				if (player.getRoom() == 5 || player.getRoom() == 6) {
					targetX = Pathfinding.door51.x;
					targetY = Pathfinding.door51.y;
				}
			}
		}

		body.setLinearVelocity(new Vector2((-(body.getPosition().x - targetX)), -(body.getPosition().y - targetY)).nor().scl(2));

	}

	public int getRoom() {
		float x = body.getPosition().x;
		float y = body.getPosition().y;

		if (x < 20) {
			if (y > 18)
				return 7;
			else
				return 6;

		}

		if (x > 51)
			return 3;
		if (y > 25 && x >= 20 && x <= 51)
			return 4;
		if (y < 15.5 && x >= 20 && x <= 51)
			return 5;
		if (y >= 15.5 && y <= 25 && x <= 51 && x > 30.5)
			return 1;
		if (y >= 15.5 && y <= 25 && x <= 30.5)
			return 2;

		return attack;

	}

	public Body getBody() {
		return body;
	}

	public void die() {
		dieLight.setActive(true);
		dieLight.setPosition(body.getPosition().x, body.getPosition().y);
	}

	public void dead() {
		dieLight.remove();
	}

	public FixtureDef getFix() {
		return fix;
	}

}
