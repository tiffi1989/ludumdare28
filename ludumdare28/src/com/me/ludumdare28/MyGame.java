package com.me.ludumdare28;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class MyGame extends Game{

	SpriteBatch batch;
	BitmapFont font;
	
	@Override
	public void create() {
		batch = new SpriteBatch();
		font = new BitmapFont(Gdx.files.internal("data/font/font2.fnt"), Gdx.files.internal("data/font/font2.png"), false);
		font.setScale(0.3f);
		font.setColor(Color.RED);
		this.setScreen(new MainMenu(this));
		
	}
	

}
