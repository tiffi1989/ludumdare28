package com.me.ludumdare28;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;

public class ContactLis implements ContactListener {

	@Override
	public void beginContact(Contact contact) {
		if (contact.getFixtureA().getBody().isBullet() || contact.getFixtureB().getBody().isBullet()) {
			if (contact.getFixtureB().getBody().getUserData() != null) {
				if (contact.getFixtureB().getBody().getUserData().equals("player"))
					contact.getFixtureB().getBody().setUserData("player_hit");
			} else
				contact.getFixtureB().getBody().setUserData("hit");

			if (contact.getFixtureA().getBody().getUserData() != null) {
				if (contact.getFixtureA().getBody().getUserData().equals("player"))
					contact.getFixtureA().getBody().setUserData("player_hit");
			} else
				contact.getFixtureA().getBody().setUserData("hit");

		}

		// if (contact.getFixtureA().getBody().getUserData().equals("player") && !contact.getFixtureB().getBody().getUserData().equals("wall")) {
		// contact.getFixtureA().getBody().setUserData("player_hit");
		// }
		// if (contact.getFixtureB().getBody().getUserData().equals("player") && !contact.getFixtureA().getBody().getUserData().equals("wall")) {
		// contact.getFixtureB().getBody().setUserData("player_hit");
		// }

		if (contact.getFixtureA().getBody().getUserData() != null && contact.getFixtureB().getBody().getUserData() != null) {
			if (contact.getFixtureA().getBody().getUserData().equals("player") && (!contact.getFixtureB().getBody().getUserData().equals("wall")))
				contact.getFixtureA().getBody().setUserData("player_hit");
			if (contact.getFixtureB().getBody().getUserData().equals("evilBullet"))
				contact.getFixtureB().getBody().setUserData("destroy");
		}
		if (contact.getFixtureA().getBody().getUserData() != null && contact.getFixtureB().getBody().getUserData() != null) {
			if (contact.getFixtureB().getBody().getUserData().equals("player") && !contact.getFixtureA().getBody().getUserData().equals("wall"))
				contact.getFixtureB().getBody().setUserData("player_hit");
			if (contact.getFixtureA().getBody().getUserData().equals("evilBullet"))
				contact.getFixtureA().getBody().setUserData("destroy");
		}
		if (contact.getFixtureA().getBody().getUserData() != null && contact.getFixtureB().getBody().getUserData() == null) {
			if (contact.getFixtureA().getBody().getUserData().equals("player"))
				contact.getFixtureA().getBody().setUserData("player_hit");
		}
		if (contact.getFixtureA().getBody().getUserData() == null && contact.getFixtureB().getBody().getUserData() != null) {
			if (contact.getFixtureB().getBody().getUserData().equals("player"))
				contact.getFixtureB().getBody().setUserData("player_hit");
		}

	}

	@Override
	public void endContact(Contact contact) {
		// TODO Auto-generated method stub

	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		// TODO Auto-generated method stub

	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
		// TODO Auto-generated method stub

	}

}
