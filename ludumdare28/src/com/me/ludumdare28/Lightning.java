package com.me.ludumdare28;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;

import box2dLight.PointLight;
import box2dLight.RayHandler;

public class Lightning {
	private PointLight lightning;
	private RayHandler handler;
	private boolean isReallyActive;
	private Sound sound;

	public Lightning(RayHandler handler) {
		this.handler = handler;
		Color color = new Color(0.8f, 0.8f, 1, 1);
		lightning = new PointLight(handler, 10, color, 500, 0, 0);
		lightning.setXray(true);
		lightning.setActive(false);
		sound =  Gdx.audio.newSound(Gdx.files.internal("data/sounds/blitz.wav")); 
	}
	
	public void startBlitz(){
		sound.play();
		blitz();
	}

	public void blitz(){
		lightning.setActive(true);
		isReallyActive = true;
				
	}
	
	public void blitzAus(){
		lightning.setActive(false);
	}
	
	public PointLight getLightning() {
		return lightning;
	}
	
	public void blitzStop(){
		isReallyActive = false;
		lightning.setActive(false);
	}
	
	public boolean isActive(){
		return isReallyActive;
	}
}
