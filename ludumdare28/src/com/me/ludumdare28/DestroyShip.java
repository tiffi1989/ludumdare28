package com.me.ludumdare28;

import com.badlogic.gdx.Gdx;

public class DestroyShip {
	Explosion ex[] = new Explosion[55];
	
	public DestroyShip() {
		reset();
	}
	
	public Explosion[] getEx() {
		return ex;
	}
	
	public void reset(){
		for (int i = 0; i < ex.length; i++) {
			ex[i] = new Explosion();
			ex[i].load(Gdx.files.internal("data/particle/explosion.p"), Gdx.files.internal("data/particle"));
			ex[i].setPosition((float) (Math.random()*980 + 200), (float) (Math.random()*520 + 100));
			ex[i].start();			
		}
	}
}
