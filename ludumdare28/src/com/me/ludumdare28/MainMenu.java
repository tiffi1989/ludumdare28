package com.me.ludumdare28;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;

public class MainMenu implements Screen {

	private OrthographicCamera camera;
	final MyGame game;
	private Texture background;
	private Ludumdare28 ld;
	private boolean gameRunning;

	public MainMenu(final MyGame game) {
		this.game = game;

		ld = new Ludumdare28(game, this);
		background = new Texture(Gdx.files.internal("data/pics/background.png"));
		camera = new OrthographicCamera();
		camera.setToOrtho(false, 1280, 720);
		camera.update();
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1f);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		camera.update();

		game.batch.begin();
		game.batch.draw(background, 0, 0);
		if (!gameRunning) {
			game.font.draw(game.batch, "Hi", 100, 130);
			game.font.draw(game.batch, "Click anywhere to begin!", 100, 100);
		}
		if (gameRunning && !ld.getGameOver()) {
			game.font.draw(game.batch, "Hi", 100, 130);
			game.font.draw(game.batch, "Click anywhere to continue!", 100, 100);
		}
		
		if(ld.getGameOver() && !ld.getWon()){
			game.font.draw(game.batch, "Hi", 100, 130);
			game.font.draw(game.batch, "Click anywhere to try again!", 100, 100);
			game.font.draw(game.batch, "GAME OVER", Gdx.graphics.getWidth()/2 -50, Gdx.graphics.getHeight() -100);
		}

		if(ld.getWon()){
			game.font.draw(game.batch, "Hi", 100, 130);
			game.font.draw(game.batch, "Click anywhere to try again!", 100, 100);
			game.font.draw(game.batch, "VICTORY!!!", Gdx.graphics.getWidth()/2 -50, Gdx.graphics.getHeight() -100);
		}
		game.batch.end();

		if (Gdx.input.isTouched() && !ld.getGameOver()) {
			game.setScreen(ld);
			gameRunning = true;
		}
		
		if(Gdx.input.isTouched() && (ld.getGameOver() || ld.getWon())){
			ld.dispose();
			ld = new Ludumdare28(game, this);
			game.setScreen(ld);
		}

	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
