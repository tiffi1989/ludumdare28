package com.me.ludumdare28;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Explosion extends ParticleEffect {

	Sound sound;
	boolean soundPlayed = true;

	public Explosion() {
		sound = Gdx.audio.newSound(Gdx.files.internal("data/sounds/blitz.wav"));
	}

	@Override
	public void draw(SpriteBatch spriteBatch, float delta) {
		if (soundPlayed)
			sound.play();
		soundPlayed = false;
		super.draw(spriteBatch, delta);
	}

	@Override
	public void reset() {
		soundPlayed = true;
		super.reset();
	}
}
