package com.me.ludumdare28;

import java.util.ArrayList;
import java.util.Iterator;

import box2dLight.ConeLight;
import box2dLight.PointLight;
import box2dLight.RayHandler;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.maps.tiled.tiles.AnimatedTiledMapTile;
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

public class Ludumdare28 implements Screen {
	private OrthographicCamera camera;
	private OrthogonalTiledMapRenderer orthogonalTiledMapRenderer;
	private TiledMap map;
	private CreateBoxWorld worldToBox;

	private Box2DDebugRenderer renderer;
	private RayHandler handler;
	private FPSLogger logger;
	private World world;
	private Body playerBody;
	private int i, c;
	private int count = 1, index =0;
	private float time, time2, time3, time4, time5, time6, startTime, endTime, time7;
	private Lightning lightning;
	private ParticleEffect antrieb, explosionGegner, death;
	private Lights lights;
	private boolean shipDestroyed, explosionDone, justOneTimePls, justOneTimePls2, justOneTimePls3, justOneTimePls4, objectiveAc, gameOver, ported, gameWon;
	private DestroyShip destroyShip;
	private Explosion ex[];
	private Double rnd = Math.random();
	private Sound shipdest1, shipdest2, laser, hit, explosionGegnerSound, telep;
	float h, w;
	private MyGame game;
	private MainMenu mainMenu;
	private ArrayList<Enemy> enemys = new ArrayList<Enemy>();
	private ArrayList<Enemy> enemysDie = new ArrayList<Enemy>();
	private ArrayList<Enemy> enemysAreDying = new ArrayList<Enemy>();
	private Player player;
	private TextureAtlas atlas;
	private Sprite playerSpriteL, playerSpriteR, bulletSprite;
	private Animation playerAniL, playerAniR, enemy1, endBossAni;
	private Array<AtlasRegion> playerAniSprites;
	private ArrayList<Body> bullets = new ArrayList<Body>();
	private ArrayList<PointLight> bulletsDestroy = new ArrayList<PointLight>();
	private ArrayList<PointLight> bulletLights = new ArrayList<PointLight>();
	private Talk talk;
	private PointLight targetLight, bossLight;
	private EndBoss endBoss;

	private ContactLis contactLis;

	public Ludumdare28(final MyGame game, MainMenu mainMenu) {
		w = Gdx.graphics.getWidth();
		h = Gdx.graphics.getHeight();

		this.game = game;
		this.mainMenu = mainMenu;
		atlas = new TextureAtlas(Gdx.files.internal("data/pics/player.atlas"));
		playerAniL = new Animation(0.2f, atlas.findRegions("playerFront"));
		playerAniSprites = atlas.findRegions("playerFront");
		for (AtlasRegion region : playerAniSprites) {
			region.flip(true, false);
		}

		talk = new Talk();
		enemy1 = new Animation(0.2f, atlas.findRegions("Enemy1"));
		endBossAni = new Animation(0.2f, atlas.findRegions("endgegner"));
		playerAniR = new Animation(0.2f, playerAniSprites);
		playerSpriteL = atlas.createSprite("playerFrontStanding");
		playerSpriteR = atlas.createSprite("playerFrontStanding");
		playerSpriteR.flip(true, false);
		bulletSprite = atlas.createSprite("bullet");

		contactLis = new ContactLis();

		map = new TmxMapLoader().load("data/world/raumschiff.tmx");
		orthogonalTiledMapRenderer = new OrthogonalTiledMapRenderer(map, 1 / 16f);

		renderer = new Box2DDebugRenderer();
		worldToBox = new CreateBoxWorld(map);

		logger = new FPSLogger();

		shipdest1 = Gdx.audio.newSound(Gdx.files.internal("data/sounds/shipdest2.wav"));
		shipdest2 = Gdx.audio.newSound(Gdx.files.internal("data/sounds/shipdest.wav"));
		laser = Gdx.audio.newSound(Gdx.files.internal("data/sounds/laser.wav"));
		hit = Gdx.audio.newSound(Gdx.files.internal("data/sounds/hit.wav"));
		explosionGegnerSound = Gdx.audio.newSound(Gdx.files.internal("data/sounds/explosionGegner.wav"));
		telep = Gdx.audio.newSound(Gdx.files.internal("data/sounds/insektenalien.wav"));

		destroyShip = new DestroyShip();

		world = new World(new Vector2(0, 0), false);

		player = new Player(world, 1100, 380);
		endBoss = new EndBoss(world, 600, 350);
		endBoss.getBody().setActive(false);
		

		playerBody = player.getBody();

		antrieb = new ParticleEffect();
		antrieb.load(Gdx.files.internal("data/particle/antrieb.p"), Gdx.files.internal("data/particle"));
		antrieb.setPosition(365, 280);
		antrieb.start();
		antrieb.findEmitter("Untitled").setContinuous(true);
		antrieb.findEmitter("Untitled2").setContinuous(true);
		antrieb.findEmitter("Untitled3").setContinuous(true);
		explosionGegner = new ParticleEffect();
		explosionGegner.load(Gdx.files.internal("data/particle/explosionGegner.p"), Gdx.files.internal("data/particle"));
		explosionGegner.start();
		death = new ParticleEffect();
		death.load(Gdx.files.internal("data/particle/death.p"), Gdx.files.internal("data/particle"));

		ex = destroyShip.getEx();

		handler = new RayHandler(world);

		bossLight = new PointLight(handler, 1000, Color.PINK, 5, 0, 0);
		bossLight.attachToBody(endBoss.getBody(), 0, 0);
		bossLight.setActive(false);
		targetLight = new PointLight(handler, 1000, Color.BLACK, 40, 15, 13);
		targetLight.setActive(false);

		lightning = new Lightning(handler);
		lights = new Lights(handler, playerBody);

		new PointLight(handler, 100, Color.YELLOW, 5, 7, (Gdx.graphics.getHeight() / 16) - 10);

		camera = new OrthographicCamera();
		camera.setToOrtho(false, 70, 39);
		camera.update();

		world = worldToBox.generateWorld(world);
		world.setContactListener(contactLis);

	}

	@Override
	public void dispose() {

		world.dispose();

	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		orthogonalTiledMapRenderer.setView(camera);
		AnimatedTiledMapTile.updateAnimationBaseTime();
		orthogonalTiledMapRenderer.render();

		logger.log();

//		renderer.render(world, camera.combined);

		world.step(1 / 60f, 6, 2);

		i += 3;
		lights.setAlarm(i);
		if (i == 360)
			i = 0;

		lights.antriebLightChange();

		float speed = 5f;
		Float oldX, oldY, newX, newY;
		oldX = playerBody.getPosition().x * 18.25f;
		oldY = playerBody.getPosition().y * 18.25f;
		newX = Gdx.input.getX() * 1f;
		newY = h - Gdx.input.getY() * 1f;

		playerBody.setTransform(playerBody.getPosition().x, playerBody.getPosition().y, (float) -(Math.PI - Math.atan2((oldY - newY), (oldX - newX))));

		if (lightning.isActive()) {
			time += Gdx.graphics.getDeltaTime();
			if (time > 0.15f) {
				lightning.blitzAus();
			}
			if (time > 0.3f) {
				lightning.blitz();
			}
			if (time > 0.45f) {
				lightning.blitzStop();
				time = 0;
			}
		}
		if (Gdx.input.isKeyPressed(Input.Keys.Y) && !lightning.isActive()){
			objectiveAc = true;
			lights.lightOff();
		}
		if (Gdx.input.isKeyPressed(Input.Keys.X))
			shipDestroyed = true;
		if (Gdx.input.isKeyPressed(Input.Keys.ESCAPE))
			game.setScreen(mainMenu);

		if (index >= 10) {
			if ((Gdx.input.isKeyPressed(Input.Keys.RIGHT) || Gdx.input.isKeyPressed(Input.Keys.D)) || (Gdx.input.isKeyPressed(Input.Keys.LEFT) || Gdx.input.isKeyPressed(Input.Keys.A))
					|| (Gdx.input.isKeyPressed(Input.Keys.UP) || Gdx.input.isKeyPressed(Input.Keys.W)) || (Gdx.input.isKeyPressed(Input.Keys.DOWN) || Gdx.input.isKeyPressed(Input.Keys.S))) {

				if ((Gdx.input.isKeyPressed(Input.Keys.RIGHT) || Gdx.input.isKeyPressed(Input.Keys.D))) {
					if ((Gdx.input.isKeyPressed(Input.Keys.UP) || Gdx.input.isKeyPressed(Input.Keys.W)) || (Gdx.input.isKeyPressed(Input.Keys.DOWN) || Gdx.input.isKeyPressed(Input.Keys.S))) {
						if ((Gdx.input.isKeyPressed(Input.Keys.UP) || Gdx.input.isKeyPressed(Input.Keys.W)))
							playerBody.setLinearVelocity(speed, speed);
						if ((Gdx.input.isKeyPressed(Input.Keys.DOWN) || Gdx.input.isKeyPressed(Input.Keys.S)))
							playerBody.setLinearVelocity(speed, -speed);
					} else
						playerBody.setLinearVelocity(speed, 0);

				}
				if ((Gdx.input.isKeyPressed(Input.Keys.LEFT) || Gdx.input.isKeyPressed(Input.Keys.A))) {
					if ((Gdx.input.isKeyPressed(Input.Keys.UP) || Gdx.input.isKeyPressed(Input.Keys.W)) || (Gdx.input.isKeyPressed(Input.Keys.DOWN) || Gdx.input.isKeyPressed(Input.Keys.S))) {
						if ((Gdx.input.isKeyPressed(Input.Keys.UP) || Gdx.input.isKeyPressed(Input.Keys.W)))
							playerBody.setLinearVelocity(-speed, speed);
						if ((Gdx.input.isKeyPressed(Input.Keys.DOWN) || Gdx.input.isKeyPressed(Input.Keys.S)))
							playerBody.setLinearVelocity(-speed, -speed);
					} else
						playerBody.setLinearVelocity(-speed, 0);

				}
				if ((Gdx.input.isKeyPressed(Input.Keys.UP) || Gdx.input.isKeyPressed(Input.Keys.W))) {
					if ((Gdx.input.isKeyPressed(Input.Keys.LEFT) || Gdx.input.isKeyPressed(Input.Keys.A)) || (Gdx.input.isKeyPressed(Input.Keys.RIGHT) || Gdx.input.isKeyPressed(Input.Keys.D))) {
						if ((Gdx.input.isKeyPressed(Input.Keys.RIGHT) || Gdx.input.isKeyPressed(Input.Keys.D)))
							playerBody.setLinearVelocity(speed, speed);
						if ((Gdx.input.isKeyPressed(Input.Keys.LEFT) || Gdx.input.isKeyPressed(Input.Keys.A)))
							playerBody.setLinearVelocity(-speed, speed);
					} else
						playerBody.setLinearVelocity(0, speed);

				}
				if ((Gdx.input.isKeyPressed(Input.Keys.DOWN) || Gdx.input.isKeyPressed(Input.Keys.S))) {
					if ((Gdx.input.isKeyPressed(Input.Keys.LEFT) || Gdx.input.isKeyPressed(Input.Keys.A)) || (Gdx.input.isKeyPressed(Input.Keys.RIGHT) || Gdx.input.isKeyPressed(Input.Keys.D))) {
						if ((Gdx.input.isKeyPressed(Input.Keys.RIGHT) || Gdx.input.isKeyPressed(Input.Keys.D)))
							playerBody.setLinearVelocity(speed, -speed);
						if ((Gdx.input.isKeyPressed(Input.Keys.LEFT) || Gdx.input.isKeyPressed(Input.Keys.A)))
							playerBody.setLinearVelocity(-speed, -speed);
					} else
						playerBody.setLinearVelocity(0, -speed);

				}

			} else {
				playerBody.setLinearVelocity(0, 0);
			}
		}

		game.batch.begin();
		antrieb.draw(game.batch, delta);

		time6 += delta;
		if (time6 > 1f && explosionDone) {
			double d = Math.random() * 50 + 11;
			enemys.add(new Enemy((int) (d), 100, world, handler, index));
			time6 = 0;
		}

		if (shipDestroyed && !explosionDone) {
			time2 += Gdx.graphics.getDeltaTime();
			for (int i = 0; i < count; i++) {
				ex[i].draw(game.batch, Gdx.graphics.getDeltaTime());

				if (count < ex.length && time2 > rnd / 4) {
					rnd = Math.random();
					count++;
					time2 = 0;
					justOneTimePls = true;
				}

				if (count % 8 == 0 && justOneTimePls) {
					lightning.blitz();
					lights.switchLight();
					justOneTimePls = false;
				}

			}
		}

		if (ex[47].isComplete() && !explosionDone) {
			explosionDone = true;
			justOneTimePls2 = true;
			justOneTimePls3 = true;
			lights.lightOff();
			time2 = 0;
		}

		if (explosionDone) {
			time2 += Gdx.graphics.getDeltaTime();
			if (time2 > 3 && justOneTimePls) {
				shipdest1.play();
				justOneTimePls = false;
			}
			if (time2 > 5 && justOneTimePls2) {
				shipdest2.play();
				justOneTimePls2 = false;
			}
			if (time2 > 7 && justOneTimePls3) {
				lights.startAlarm();
				justOneTimePls3 = false;
				lights.switchFlashlight();
			}

		}

		time3 += Gdx.graphics.getDeltaTime();
		if (time3 > 1f) {
			for (Enemy enemy : enemys) {
				enemy.move(player);
			}
			time3 = 0;
		}

		time4 += delta;

		time5 += delta;
		if (Gdx.input.isTouched() && time5 > 0.1f) {
			BodyDef bulletDef = new BodyDef();
			bulletDef.type = BodyType.DynamicBody;
			if (player.getPosX() * 18.25f > Gdx.input.getX())
				bulletDef.position.set(player.getPosX() - 1f, player.getPosY());
			else
				bulletDef.position.set(player.getPosX() + 1, player.getPosY());

			laser.play();
			Body bulletbody = world.createBody(bulletDef);
			bullets.add(bulletbody);

			CircleShape shape = new CircleShape();
			shape.setRadius(0.000000001f);

			FixtureDef fix = new FixtureDef();
			fix.shape = shape;
			fix.density = 0.01f;
			fix.friction = 0f;
			fix.restitution = 0f;

			bulletbody.createFixture(fix);
			// bulletbody.setUserData("bullet");
			bulletbody.setBullet(true);
			bulletbody.setLinearVelocity(new Vector2(Gdx.input.getX() - player.getPosX() * 18.25f, h - Gdx.input.getY() - player.getPosY() * 18.25f).nor().scl(20));
			PointLight bulletLight = new PointLight(handler, 100, new Color(0, 0.5f, 0, 0.5f), 3, 0, 0);
			bulletLight.attachToBody(bulletbody, 0, 0);

			bulletLights.add(bulletLight);

			// justOneTimePls4 = true;
			time5 = 0;

		}
		time7 += delta;

		if (time7 > 0.3f && ported && endBoss.getLifes() >0) {
			BodyDef bulletDef = new BodyDef();
			bulletDef.type = BodyType.DynamicBody;

			bulletDef.position.set(endBoss.getPosX() + 3, endBoss.getPosY() - 1);

			laser.play();
			Body bulletbody = world.createBody(bulletDef);
			bulletbody.setUserData("evilBullet");
			bullets.add(bulletbody);

			CircleShape shape = new CircleShape();
			shape.setRadius(0.000000001f);

			FixtureDef fix = new FixtureDef();
			fix.shape = shape;
			fix.density = 0.01f;
			fix.friction = 0f;
			fix.restitution = 0f;

			bulletbody.createFixture(fix);
			// bulletbody.setUserData("bullet");
			bulletbody.setLinearVelocity(new Vector2(player.getPosX() - endBoss.getPosX(), player.getPosY() - endBoss.getPosY() + 1).nor().scl(20));
			PointLight bulletLight = new PointLight(handler, 100, Color.RED, 3, 0, 0);
			bulletLight.attachToBody(bulletbody, 0, 0);

			bulletLights.add(bulletLight);

			// justOneTimePls4 = true;
			time7 = 0;

		}

		for (PointLight bulletLight : bulletLights) {
			if (bulletLight.getBody().getUserData() != null) {
				if (!bulletLight.getBody().getUserData().equals("evilBullet")) {
					world.destroyBody(bulletLight.getBody());
					bulletsDestroy.add(bulletLight);
				}
			}
		}

		for (PointLight bullet : bulletsDestroy) {
			hit.play();
			bullets.remove(bullet.getBody());
			bulletLights.remove(bullet);
			bullet.remove();
		}
		bulletsDestroy = new ArrayList<PointLight>();

		for (Enemy enemy : enemys) {
			if (enemy.getLifes() < 0 || ported)
				enemysDie.add(enemy);
		}

		explosionGegner.draw(game.batch, delta);

		for (Enemy enemy : enemysDie) {
			explosionGegner.reset();
			explosionGegner.setPosition(enemy.getPosX() * 18.25f, enemy.getPosY() * 18.25f);
			enemys.remove(enemy);
			explosionGegnerSound.play();
			world.destroyBody(enemy.getBody());
			enemysAreDying.add(enemy);
			enemy.die();
		}

		if (explosionGegner.isComplete()) {
			for (Enemy enemy : enemysAreDying) {

				enemy.dead();

			}
			enemysAreDying = new ArrayList<Enemy>();
		}

		enemysDie = new ArrayList<Enemy>();

		if (!gameOver) {
			if (player.getPosX() * 18.25f > Gdx.input.getX()) {
				if (player.getBody().getLinearVelocity().len() == 0)
					game.batch.draw(playerSpriteL, (player.getPosX() - 1.25f) * 18.25f, (player.getPosY() - .7f) * 18.25f, 0, 0, 32, 32, 1.5f, 1.5f, 0);
				else
					game.batch.draw(playerAniL.getKeyFrame(time4, true), (player.getPosX() - 1.25f) * 18.25f, (player.getPosY() - .7f) * 18.25f, 0, 0, 32, 32, 1.5f, 1.5f, 0);
			} else {
				if (player.getBody().getLinearVelocity().len() == 0)
					game.batch.draw(playerSpriteR, (player.getPosX() - 1.25f) * 18.25f, (player.getPosY() - .7f) * 18.25f, 0, 0, 32, 32, 1.5f, 1.5f, 0);
				else
					game.batch.draw(playerAniR.getKeyFrame(time4, true), (player.getPosX() - 1.25f) * 18.25f, (player.getPosY() - .7f) * 18.25f, 0, 0, 32, 32, 1.5f, 1.5f, 0);
			}
		}

		for (Enemy enemy : enemys) {
			game.batch.draw(enemy1.getKeyFrame(time4, true), (enemy.getPosX() - .75f) * 18.25f, (enemy.getPosY() - .6f) * 18.25f, 0.85f * 18.25f, 0.85f * 18.25f, 32, 32, 1f, 1f, time4 * 70);
			if (enemy.getBody().getUserData() != null) {
				enemy.setLifes(enemy.getLifes() - 10);
				enemy.getBody().setUserData(null);
			}
		}
		if (player.getBody().getUserData().equals("player_hit")) {
			player.setLifes(player.getLifes() - 1);
			player.getBody().setUserData("player");
		}
		for (Body bullet : bullets) {
			if (bullet.getUserData() != null)
				game.batch.draw(bulletSprite, bullet.getPosition().x * 18.25f, bullet.getPosition().y * 18.25f, 0, 0, 6, 2, 1, 1, 0);
			else
				game.batch.draw(bulletSprite, bullet.getPosition().x * 18.25f, bullet.getPosition().y * 18.25f, 0, 0, 6, 2, 1, 1,
						(float) -((Math.PI - Math.atan2((oldY - newY), (oldX - newX))) * 180 / Math.PI));

		}

		if (endBoss.getBody().getUserData().equals("hit")) {
			endBoss.setLifes(endBoss.getLifes() - 10);
			endBoss.getBody().setUserData("notHit");
		}

		if (endBoss.getLifes() < 0) {
			endTime += delta;
			death.start();
			death.draw(game.batch, delta);
			death.setPosition(endBoss.getPosX() * 18.25f, endBoss.getPosY() * 18.25f);
			lights.lightOn();
			gameWon = true;
		}
		handler.setCombinedMatrix(camera.combined, camera.position.x, camera.position.y, camera.viewportWidth * camera.zoom, camera.viewportHeight * camera.zoom);

		if (player.getLifes() < 0) {
			endTime += delta;
			death.start();
			death.draw(game.batch, delta);
			death.setPosition(player.getPosX() * 18.25f, player.getPosY() * 18.25f);
			lights.lightOn();
			gameOver = true;
		}

		if (index == 26 && !ported) {
			TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get(1);
			layer.getCell(1, 1);
			for (int x = 0; x < layer.getWidth(); x++) {
				for (int y = 0; y < layer.getHeight(); y++) {
					if (layer.getCell(x, y).getTile().getProperties().containsKey("door")) {
						BodyDef rectDef = new BodyDef();
						rectDef.type = BodyType.StaticBody;
						rectDef.position.set(x + 0.5f, y + 0.5f);

						Body rect = world.createBody(rectDef);
						rect.setUserData("wall");

						PolygonShape polyShape = new PolygonShape();
						polyShape.setAsBox(0.5f, 0.5f);

						FixtureDef rectFix = new FixtureDef();
						rectFix.shape = polyShape;

						rect.createFixture(rectFix);

						TiledMapTile tiel = null;

						Iterator<TiledMapTile> iterator = map.getTileSets().getTileSet("level").iterator();
						while (iterator.hasNext()) {
							TiledMapTile tile = iterator.next();
							if (tile.getProperties().containsKey("block"))
								tiel = tile;
						}

						layer.getCell(x, y).setTile(tiel);
					}

				}
			}

			player.getBody().setTransform(47, 22, 0);
			telep.play();

			ported = true;
			endBoss.getBody().setActive(true);
			endBoss.getBody().setLinearVelocity(0, 3f);
			bossLight.setActive(true);

		}

		if (ported && endBoss.getLifes() >0) {
			game.batch.draw(endBossAni.getKeyFrame(time4, true), (endBoss.getPosX() - 1.5f) * 18.25f, (endBoss.getPosY() - 1.5f) * 18.25f, 0, 0, 64, 64, 1, 1, 0);
		}

		if (endTime > 5) {
			game.setScreen(mainMenu);
		}

		startTime += delta;

		if (startTime > 1 && index <= 10) {
			if (index == 1 && !lightning.isActive() && startTime > 3) {
				lightning.startBlitz();
			}

			if (index == 3) {
				if (startTime > 3)
					shipDestroyed = true;

				if (startTime > 4) {
					shipDestroyed = false;
					lights.lightOff();
				}
			}
			if (index == 6) {
				shipDestroyed = true;
			}
			game.font.draw(game.batch, talk.aiTalk(index), 200, 60);
			if (startTime > 2.5) {
				game.font.draw(game.batch, talk.youTalk(index), 200, 30);
			}
			if (startTime > 4 && index < 10) {
				startTime = 0;
				index++;
			}
		}

		if (index >= 10) {
			if (index == 10 && !targetLight.isActive()) {
				targetLight.setActive(true);
			}
			if (index == 11)
				targetLight.setActive(false);
			if (index == 12) {
				targetLight.setPosition(860 / 18.25f, (h - 111) / 18.25f);
				targetLight.setActive(true);
			}
			if (index == 13) {
				targetLight.setActive(false);
			}
			if (index == 14) {
				targetLight.setPosition(470 / 18.25f, (h - 293) / 18.25f);
				targetLight.setActive(true);
			}
			if (index == 15)
				targetLight.setActive(false);
			if (index == 17 && startTime > 2.5) {
				targetLight.setPosition(311 / 18.25f, (h - 122) / 18.25f);
				targetLight.setActive(true);
			}
			if (index == 18) {
				targetLight.setActive(false);
			}
			if (index == 22 && player.getPosX() * 18.25f > 1122 && !objectiveAc) {
				objectiveAc = true;
				startTime = 0;
				index++;
			}
			if (player.getRoom() == 7 && index == 17 && !objectiveAc) {
				objectiveAc = true;
				startTime = 0;
				index++;
			}
			if (player.getRoom() == 6 && index == 10 && !objectiveAc) {
				objectiveAc = true;
				startTime = 0;
				index++;
			}
			if (player.getPosX() * 18.25f > 812 && player.getPosY() * 18.25f > h - 140 && index == 12 && !objectiveAc) {
				objectiveAc = true;
				startTime = 0;
				index++;
			}
			if (player.getRoom() == 2 && index == 14 && !objectiveAc) {
				objectiveAc = true;
				startTime = 0;
				index++;
			}
			if (startTime > 1 && index >= 11) {
				game.font.draw(game.batch, talk.aiTalk(index), 200, 60);
				if (startTime > 2.5) {
					game.font.draw(game.batch, talk.youTalk(index), 200, 30);
				}

				if (index != 21) {
					if (startTime > 4 && objectiveAc) {
						startTime = 0;
						if (index != 12 && index != 14)
							index++;
						if (index == 12 || index == 14 || index == 17 || index == 18 || index == 22) {
							objectiveAc = false;

						}
					}
				} else {
					if (startTime > 5 && objectiveAc) {
						startTime = 0;
						index++;
						objectiveAc = false;
					}
				}
			}
		}

		System.out.println(player.getRoom());
		if (gameOver)
			game.font.draw(game.batch, "lifes: not ONE", 100, 100);
		else
			game.font.draw(game.batch, "lifes: " + player.getLifes(), 100, 100);

		if (endBoss.getBody().isActive() && endBoss.getLifes() > 0)
			game.font.draw(game.batch, "lifes: " + endBoss.getLifes(), 100, 150);
		if(endBoss.getLifes() < 0)
			game.font.draw(game.batch, "lifes:  NOT ONE", 100, 150);
//		game.font.draw(game.batch, Gdx.input.getX() + " " + Gdx.input.getY(), 100, 150);
//		game.font.draw(game.batch, index + "", 100, 200);
		game.batch.end();

		handler.updateAndRender();

		camera.update();
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	public boolean getGameOver() {
		return gameOver;
	}
	
	public boolean getWon(){
		return gameWon;
	}

}
