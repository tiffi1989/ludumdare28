package com.me.ludumdare28;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

public class EndBoss {
	int posX, posY;

	int lifes = 1000;
	int attack;
	Body body;
	
	public EndBoss(World world, int posX, int posY) {		
		BodyDef playerDef = new BodyDef();
		playerDef.type = BodyType.DynamicBody;
		playerDef.position.set(posX / 18.25f, posY / 18.25f);

		body = world.createBody(playerDef);
		body.setUserData("EndBoss");
		body.setFixedRotation(true);

		PolygonShape polyShape = new PolygonShape();
		polyShape.setAsBox(1.8f, 1.8f);

		FixtureDef fix = new FixtureDef();
		fix.shape = polyShape;
		fix.density = 100f;
		fix.friction = 0f;
		fix.restitution = 1f;
		

		body.createFixture(fix);
	
	}
	
	public float getPosX() {
		return body.getPosition().x;
	}
	
	public float getPosY() {
		return body.getPosition().y;
	}
	
	public int getLifes() {
		return lifes;
	}
	public void setLifes(int lifes) {
		this.lifes = lifes;
	}
	public Body getBody() {
		return body;
	}
	public void setBody(Body body) {
		this.body = body;
	}
	
	
}
