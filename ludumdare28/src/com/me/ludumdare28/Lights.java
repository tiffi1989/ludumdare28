package com.me.ludumdare28;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.physics.box2d.Body;

import box2dLight.ConeLight;
import box2dLight.PointLight;
import box2dLight.RayHandler;

public class Lights {

	private Body playBody;
	private RayHandler handler;
	private ConeLight alarm, alarm2;
	private PointLight antriebLight, sun6, playerLight;
	private float antriebStärke = 0.7f;
	private boolean up = true;
	private boolean started;
	private ConeLight flashlight;

	public Lights(RayHandler handler, Body playerBody) {

		this.handler = handler;
		this.playBody = playerBody;

		flashlight = new ConeLight(handler, 500, Color.BLACK, 15, 0, 0, 0, 30);
		flashlight.attachToBody(playerBody, 0, 0);
		flashlight.setActive(false);
		playerLight = new PointLight(handler, 100, Color.BLACK, 4, 0, 0);
		playerLight.attachToBody(playerBody, 0, 0);
		playerLight.setActive(false);
		

		PointLight sun = new PointLight(handler, 100, Color.BLACK, 1000, 5, (Gdx.graphics.getHeight() / 16) - 10);
		PointLight sun2 = new PointLight(handler, 100, Color.BLACK, 1000, 70, 0);
		PointLight sun3 = new PointLight(handler, 100, Color.BLACK, 1000, 70, 39);
		PointLight sun4 = new PointLight(handler, 100, Color.BLACK, 1000, 0, 39);
		PointLight sun5 = new PointLight(handler, 100, Color.BLACK, 1000, 35, 0);
		sun6 = new PointLight(handler, 100, Color.BLACK, 1000, 35, 39);
		sun6.setXray(true);
		antriebLight = new PointLight(handler, 5, new Color(0, 1, 1, 1), 40, 18, 20);

	}

	public void setAlarm(int i) {
		if (started) {
			alarm.setDirection(i);
			alarm2.setDirection(180 + i);
		}
	}

	public void startAlarm() {
		alarm = new ConeLight(handler, 500, new Color(1f, 0f, 0, 1), 20, 58, 20, 0, 30);
		alarm2 = new ConeLight(handler, 500, new Color(1f, 0f, 0, 1), 20, 58, 20, 0, 30);
		started = true;
	}

	public void switchLight() {
		if (sun6.isXray())
			sun6.setXray(false);
		else
			sun6.setXray(true);
	}
	
	public void lightOff(){
		sun6.setXray(false);
	}
	
	public void lightOn(){
		sun6.setXray(true);
	}

	public void antriebLightChange() {

		if (antriebStärke > 0.99f)
			up = false;
		if (antriebStärke < 0.7f)
			up = true;
		if (up) {
			antriebStärke += 0.01f;
		} else
			antriebStärke -= 0.01f;

		antriebLight.setColor(0, 1, 1, antriebStärke);

	}

	public void switchFlashlight(){
		if(flashlight.isActive()){
			flashlight.setActive(false);
			playerLight.setActive(false);
		}else{
			flashlight.setActive(true);
			playerLight.setActive(true);
		}
	}
}
