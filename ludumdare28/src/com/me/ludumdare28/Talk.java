package com.me.ludumdare28;

import com.badlogic.gdx.utils.Array;

public class Talk {
	Array<String> you = new Array<String>(50);
	Array<String> ai = new Array<String>(50);

	public Talk() {
		ai.add("AI: I think... we might have a problem");
		you.add("You: You can fix this, right? ");
		ai.add("AI: Sure... sure... no Problem");
		you.add("AI: Uuuuuupss...");
		ai.add("You: What was that?");
		you.add("AI: What was what?");
		ai.add("You: Stupid thing...");
		you.add("AI: Does not compute!");
		ai.add("You: AI... The lights are off.");
		you.add("You: AI... NOT FUNNY");
		ai.add("AI: I think I just found the defect");
		you.add("AI: Just one last operation");
		ai.add("");
		you.add("");
		ai.add("");
		you.add("");
		ai.add("AI: Sir, the possibility of surviving is approximately 3,720 to 1.");
		you.add("You: Never tell me the odds!!");
		ai.add("");
		you.add("");
		ai.add("AI: Sir, would you consider checking the Toilet for me?");
		you.add("You: ... Sure... The Toilet");
		ai.add("You: AI, there is.... Nothing");
		you.add("AI: Yep, just wanted to double check");
		ai.add("You: I hate you.");
		you.add("AI: Just figured it out. The O2 is leaking, fix it");
		ai.add("You: Fixed it. By the way, why are the lightsaber trainingsbots everywhere?");
		you.add("AI: Don't know, don't care.");
		ai.add("AI: Oh and about this little we're all going to die thing");
		you.add("AI: The problem is the engine, would you mind?");
		ai.add("You: She is a nice AI... She is a nice AI.. She is a nice AI");
		you.add("AI: I'm not really sure if you're speaking with me?!");
		ai.add("AI: Your stress level is a little bit concerning ");
		you.add("AI: Just go to the fridge, grab a beer and enjoy the nice evening.");
		ai.add("You: What about these killer trainingsrobots?");
		you.add("AI: FRIDGE! NOW! BEER! NOW! BITCH!");
		ai.add("You: Ok, I got a beer, sooo please, just help me fix it.");
		you.add("AI: No");
		ai.add("You(slightly angered): What?");
		you.add("AI: No!");
		ai.add("You(angered): Then shut up and let me figure it out.");
		you.add("AI: No!");
		ai.add("You(enraged): Ok");
		you.add("You(now multiclassed nerd/babarian warrior): OK SHUT UP OR SHUT OFF!");
		ai.add("--- Go to the bridge and deaktivate this bitch ---");
		you.add("");
		ai.add("You: ahhh... Golden silence");
		you.add("AI: *silence*");
		ai.add("Crazy AI: YOU THINK YOU JUST TURN ME OFF?");
		you.add("Batcrap Crazy AI: DIE!");
		ai.add("Crazy AI: Activate teleporter!");
		you.add("Crazy AI: Close Doors");
		ai.add("");
		you.add("");
		ai.add("");
		you.add("");
		ai.add("");
		you.add("");
		ai.add("");
		you.add("");

	}

	public String youTalk(int index) {
		if (index <= 27)
			return you.get(index);
		else
			return "";
	}

	public String aiTalk(int index) {
		if (index <= 27)
			return ai.get(index);
		else
			return "";
	}

}
